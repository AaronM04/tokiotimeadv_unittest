use std::time::Duration;
use tokio::sync::oneshot;
use tokio::time::{self, timeout_at, Instant};

const DO_SEND: bool = true; ////////// <------ toggle this

#[tokio::test]
async fn time_advance_test() {
    time::pause();
    let (tx, rx) = oneshot::channel::<i64>();

    let start = Instant::now();
    let expiration = start + Duration::from_secs(3);

    if DO_SEND {
        tx.send(3).expect("send fail");
    }

    time::advance(Duration::from_secs(5)).await;

    let timeout_result = timeout_at(expiration, rx).await;
    let end = Instant::now();

    // The `timeout_result` should be Ok(...), meaning no timeout, if we sent something on the
    // channel. It should be Err(...), meaning a timeout occurred if nothing was sent on the
    // channel.
    assert_eq!(timeout_result.is_ok(), DO_SEND);

    println!("Time travelled: {:?}", end - start);
}
